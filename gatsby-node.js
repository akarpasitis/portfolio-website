/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.com/docs/node-apis/
 */

// You can delete this file if you're not using it
// createPage({
//   path: "/art",
//   matchPath: "/art/:id",
//   component: path.resolve(`src/page/art.js`)
// })
const path =  require("path")

exports.onCreatePage = async ({ page, actions }) => {
  const { createPage } = actions

  // page.matchPath is a special key that's used for matching pages
  // only on the client.
  createPage({
    path: "/art/:id",
    matchPath: "/art/:id",
    component: path.resolve(`src/components/art.js`)
  })

  createPage({
    path: "/art",
    matchPath: "/art",
    component: path.resolve(`src/components/art.js`)
  })
}
