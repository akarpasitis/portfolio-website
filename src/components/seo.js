/**
 * SEO component that queries for data with
 *  Gatsby's useStaticQuery React hook
 *
 * See: https://www.gatsbyjs.com/docs/use-static-query/
 */

import React from "react"
import PropTypes from "prop-types"
import { Helmet } from "react-helmet"
import { useStaticQuery, graphql } from "gatsby"
import image from '../assets/ogimage.jpg'
import bookimage from '../assets/bookimageog.jpg'

function SEO({ description, isBook = false, lang, meta, keywords, url, title }) {
  const { site } = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            title
            description
            author
          }
        }
      }
    `
  )

  const metaDescription = description || site.siteMetadata.description
  const defaultTitle = site.siteMetadata?.title

  return (
    <Helmet
      htmlAttributes={{
        lang
      }}
      title={title}
      titleTemplate={defaultTitle ? `%s | ${defaultTitle}` : null}
      meta={[
        {
          name: `description`,
          content: metaDescription
        },
        {
          name: `keywords`,
          content: keywords
        },
        {
          name: `author`,
          content: 'Andreas Karpasitis'
        },
        {
          name: `url`,
          content: url
        },
        {
          property: `og:title`,
          content: `${title} - Andreas Karpasitis`
        },
        {
          property: `og:description`,
          content: metaDescription
        },
        {
          property: `og:type`,
          content: `website`
        },
        {
          property: `og:image`,
          content: `https://www.karpasitis.net${isBook ? bookimage : image}`
        },
        {
          property: `og:url`,
          content: url
        },
        {
          name: `twitter:card`,
          content: `summary_large_image`
        },
        {
          name: `twitter:creator`,
          content: site.siteMetadata?.author || `Andreas Karpasitis`
        },
        {
          name: `twitter:image`,
          content: `https://www.karpasitis.net${isBook ? bookimage : image}`
        },
        {
          name: `twitter:title`,
          content: `${title} - Andreas Karpasitis`
        },
        {
          name: `twitter:description`,
          content: metaDescription
        }
      ].concat(meta)}
    >
      <link rel="canonical" href={url} />
    </Helmet>
  )
}

SEO.defaultProps = {
  lang: `en`,
  meta: [],
  description: ``
}

SEO.propTypes = {
  description: PropTypes.string,
  lang: PropTypes.string,
  meta: PropTypes.arrayOf(PropTypes.object),
  title: PropTypes.string.isRequired
}

export default SEO
