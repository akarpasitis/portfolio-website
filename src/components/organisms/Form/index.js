import React, { Component } from "react"
import style from "./index.scss"
import { Button } from 'semantic-ui-react'

class Form extends Component {
  state = {
  }

  render() {
    return (
      <div className={style.container}>
        <h3 className={style.text}>
          If you wish to know rates for any project, including photography and videography get in touch!
          Looking forward to hear about new creative projects, collaborations, ideas and opportunities.
        </h3>
        <div className={style.formContainer}>
          <form action="" className={style.form} name="contact" method="post">
            <input type="hidden" name="form-name" value="contact" />
            <p>
              <label>Your Name: <input type="text" name="name" /></label>
            </p>
            <p>
              <label>Your Email: <input type="email" name="email" /></label>
            </p>
            <p>
              <label>Subject:  <input type="text" name="subject" /></label>
            </p>
            <p>
              <label>Message: <textarea name="message"></textarea></label>
            </p>
            <Button className={style.submit} type="submit">Submit</Button>
          </form>
        </div>
        <h3>Or send me an email</h3>
        <a href="mailto:info@karpasitis.net">
          <Button className={style.button} >Send Email</Button>
        </a>
        <h4 className={style.text}><b>Current Location:</b><em> Barcelona, Spain</em></h4>
      </div>
    )
  }
}

export default Form