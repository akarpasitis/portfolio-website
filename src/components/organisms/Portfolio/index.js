import React, { Component } from "react"
import style from "./index.scss"
import _ from "lodash"
import { Link } from "react-router-dom"
import LazyLoad from "react-lazy-load"
import { Helmet } from "react-helmet"

class Portfolio extends Component {
	state = {
		isMobile: false
	}

	componentDidMount() {
		if (window.innerWidth < 650) {
			this.setState({ isMobile: true })
		} else {
			this.setState({ isMobile: false })
		}
	}

	importAll = data => {
		let images = {}
		data.keys().map((item, index) => {
			return (images[item.replace("./", "")] = data(item))
		})
		return images
	}

	render() {
		const images = this.importAll(
			require.context(
				"../../../assets/covers/art/compressed/",
				false,
				/\.(png|jpe?g|svg)$/
			)
		)
		const names = [
			"Gabriella",
			"Angelina",
			"Gabriella",
			"Darina",
			"Georgia",
			"Darina",
			"Heidi",
			"Gabriella",
			"Kate",
			"Kayla",
			"Heidi",
			"Lucia",
			"VikTory",
			"Lucia",
			"Kate",
			"Maria",
			"Noelia"
		]

		return (
			<React.Fragment>
				<Helmet>
					<title>Andreas Karpasitis - Art & Fashion Photography</title>
					<link
						rel="canonical"
						href="https://www.karpasitis.net/art/"
					/>
					<meta
						name="description"
						content="Artistic, Fashion, Editorial Photography Portfolio by Andreas Karpasitis"
					/>
					<meta
						name="keywords"
						content="Karpasitis, Andreas, Photography, Photographer, Photo, Fotografia, Barcelona, Fashion, Editorial, Art, Artistic"
					/>
					<meta name="twitter:title" content="Andreas Karpasitis - Art & Fashion Photography" />
					<meta name="twitter:description" content="Artistic, Fashion, Editorial Photography Portfolio by Andreas Karpasitis" />
					<meta property="og:title" content="Andreas Karpasitis - Art & Fashion Photography" />
					<meta property="og:description" content="Artistic, Fashion, Editorial Photography Portfolio by Andreas Karpasitis" />
					<meta property="og:url" content="https://www.karpasitis.net/art/" />
				</Helmet>
        <div className={style.container}>
          <h1 className={style.titleHeader}>Art & Fashion Photography</h1>
					<div className={style.photos} id="photos">
						{_(images)
							.filter(i => i.includes("thumb"))
							.map((image, index) => {
								const imageIndex = index + 1
								const imageNumber =
									imageIndex < 10
										? "0" + imageIndex
										: imageIndex
								return (
									<Link
										key={image}
										to={`/art/${imageNumber}`}
									>
										<div
											key={`${image}`}
											className={style.link}
										>
											<LazyLoad offsetVertical={300}>
												<img
													key={`${image}-img`}
													src={image}
													alt={names[index]}
												/>
											</LazyLoad>
											{!this.state.isMobile && (
												<div className={style.title}>
													{names[index]}
												</div>
											)}
										</div>
									</Link>
								)
							})
							.value()}
					</div>
				</div>
			</React.Fragment>
		)
	}
}

export default Portfolio
